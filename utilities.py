#from zeep import Client
import cx_Oracle
# Connect as user "hr" with password "welcome" to the "oraclepdb" service running on this computer.
connection = cx_Oracle.connect("itco", "itco", "192.168.16.5/elexito")
class Utilities:
    def obtener_tipocambio(self, fecha_inicio, fecha_final):
        client = Client("https://gee.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx?wsdl")
        result = client.service.ObtenerIndicadoresEconomicos(318, fecha_inicio, fecha_final, "ITCO azuniga@itcoint.com", "N")
        return result._value_1._value_1[0]["INGC011_CAT_INDICADORECONOMIC"]["NUM_VALOR"]

    def get_tax(self, productCode):
        query = """
        select b.porcentaje
        from arinia  a, arcgimp b
        where a.no_cia = 'B1'
        and a.no_arti = :noarti
        and b.no_cia = a.no_cia
        and b.clave  = a.clave
        and a.afecta_venta = 'S'
        """
        cursor = connection.cursor()
        cursor.execute(query, noarti=productCode)
        tax = cursor.fetchone()
        cursor.close()
        return tax[0]
    
    def get_id_type(self,cedula):
        query = """
        SELECT CLIENTE.NOMBRE, CLIENTE.ID_IDENTIFICACION, IDENTIFICACION.ETIQUETA 
        FROM PVCLIENTES CLIENTE , ARGETID IDENTIFICACION 
        WHERE CLIENTE.TIPO_IDENTIFICACION = IDENTIFICACION.CODIGO
        AND cliente.id_identificacion = :cedula
        """
        cursor = connection.cursor()
        cursor.execute(query, cedula=cedula)
        tipo = cursor.fetchone()
        cursor.close()
        return tipo[2]
    
    def get_header_lines(self, transNo):
        lineas = []
        query = """
        SELECT pvlineas_movimiento.linea, 
        pvlineas_movimiento.no_transa_mov, 
        arinda.descripcion, 
        pvlineas_movimiento.no_arti, 
        pvlineas_movimiento.cantidad, 
        pvlineas_movimiento.precio, 
        pvlineas_movimiento.monto_bruto, 
        pvlineas_movimiento.descuento, 
        pvlineas_movimiento.ind_gravado, 
        pvlineas_movimiento.monto_imp, 
        pvlineas_movimiento.total_neto, 
        pvlineas_movimiento.precio_publico, 
        pvlineas_movimiento.mon_imp_publico,
        pvlineas_movimiento.mon_bruto_publico
    FROM pvlineas_movimiento 
        inner join arinda 
                ON pvlineas_movimiento.no_arti = arinda.no_arti 
    WHERE pvlineas_movimiento.no_cia = 'B1'
    AND pvlineas_movimiento.no_transa_mov = :transaccion
    and arinda.no_cia = 'B1'
    ORDER BY LINEA ASC
        """
        cursor = connection.cursor()
        cursor.execute(query, transaccion=transNo)
        resultado=cursor.fetchall()
        for linea in resultado:
            lineas.append(linea)
        return lineas
        cursor.close()

    def get_invoices(self, fecha):
        query = """
            SELECT 
            tipo_movimiento,
            no_fisico, 
            fecha, 
            no_transa_mov,
            centrod, 
            pagara_con_tarjeta, 
            cod_cliente, 
            total_bruto, 
            total_imp, 
            tipo_cambio, 
            total_exento, 
            total_gravado, 
            total,
            redondeo,
            total_descuento,
            fe_consecutivo,
            fe_clave,
            ind_venta_contado
        FROM   pvencabezado_movimientos 
        WHERE FECHA = :date_of_invoices 
        AND NO_CIA = 'B1'
        AND FECHA = TRUNC(SYSDATE)
        AND  ( (FE_CLAVE IS NULL AND FE_CONSECUTIVO IS NULL) OR (FE_CONSECUTIVO = 'PROVISIONAL' AND FE_CLAVE = 'PROVISIONAL') )
        AND (TIPO_MOVIMIENTO = 'F' OR TIPO_MOVIMIENTO = 'NC')
        AND TOTAL > 0
        """
        cursor = connection.cursor()
        cursor.execute(query, date_of_invoices = fecha)
        resultado  = cursor.fetchall()
        count = str(cursor.rowcount)
        facturas = []
        for factura in resultado:
            facturas.append(factura)
        cursor.close()
        return facturas

    def get_customer_info(self, id_customer):
        query = """
        SELECT 
            cliente.nombre, 
            cliente.id_identificacion, 
            cliente.tipo_identificacion,
            cliente.email1,
            ident.etiqueta
            FROM pvclientes cliente , argetid ident
            WHERE cod_cliente = :cod_cliente 
            AND no_cia = 'B1' 
            AND cliente.tipo_identificacion = ident.codigo
        """
        cursor = connection.cursor()
        cursor.execute(query, cod_cliente = id_customer)
        resultado  = cursor.fetchone()
        count = str(cursor.rowcount)
        cursor.close()
        return(resultado)

    def get_credit_term(self,no_transa):
        query = """
        SELECT FECHA, FECHA_VENCE FROM ARCCMD where NO_CIA = 'B1' 
        AND NO_DOCU = :transa
        """
        cursor = connection.cursor()
        cursor.execute(query, transa = no_transa)
        resultado  = cursor.fetchone()
        count = str(cursor.rowcount)
        fecha_inicio = resultado[0]
        fecha_fin = resultado[1]
        diff = fecha_fin - fecha_inicio
        days = diff.days
        return days

    def status(self,clave,consecutivo,no_transa):
        query = """
            UPDATE pvencabezado_movimientos
            SET FE_CONSECUTIVO = :FE_CONSECUTIVO,
            FE_CLAVE = :FE_CLAVE
            WHERE NO_TRANSA_MOV = :NO_TRANSA_MOV
            AND NO_CIA = 'B1'
        """
        cursor = connection.cursor()
        cursor.execute(query, FE_CONSECUTIVO = consecutivo, FE_CLAVE=clave, NO_TRANSA_MOV = no_transa)
        connection.commit()
        print("Encabezado Insertado", consecutivo , clave)
        cursor.close()
        return True
    def get_nc_reason(self,no_transa):
        try:
            query = """
            SELECT pvrazones.descripcion, pvencabezado_movimientos.no_refe_dev
            FROM pvrazones 
            INNER JOIN pvencabezado_movimientos
            ON pvrazones.razon = pvencabezado_movimientos.razon
            where pvrazones.no_cia = 'B1' and pvencabezado_movimientos.no_cia = 'B1'
            AND pvencabezado_movimientos.no_transa_mov = :NO_TRANSA_MOV
            """
            cursor = connection.cursor()
            cursor.execute(query, NO_TRANSA_MOV= no_transa)
            resultado  = cursor.fetchone()
            
            referencia = resultado[1]
            motivo = resultado[0]

            query = """
            SELECT pvencabezado_movimientos.fe_clave, pvencabezado_movimientos.fe_consecutivo
            FROM PVENCABEZADO_MOVIMIENTOS
            where pvencabezado_movimientos.no_cia = 'B1'
            AND pvencabezado_movimientos.no_transa_mov = :NO_TRANSA_MOV
            AND pvencabezado_movimientos.fe_clave IS NOT NULL
            AND pvencabezado_movimientos.fe_consecutivo IS NOT NULL
            """
            cursor = connection.cursor()
            cursor.execute(query, NO_TRANSA_MOV= referencia)
            resultado2  = cursor.fetchone()

            fe_clave = resultado2[0]
            fe_consecutivo = resultado2[1]
            print((motivo,referencia,fe_clave,fe_consecutivo))
            return(motivo,referencia,fe_clave,fe_consecutivo)

        except Exception as e:
            print(str(e))

    # def get_arccm(self,fecha):
    #     try:
    #         query = """
    #             SELECT no_docu,
    #                 centro,
    #                 no_cliente,
    #                 fecha
    #             FROM  arccmd
    #             WHERE  fecha = :fecha
    #                 AND no_cia = 'B1'
    #                 AND tipo_doc = 'NC'
    #         """
    #         cursor = connection.cursor()
    #         cursor.execute(query, fecha= fecha)
    #         resultado  = cursor.fetchall()
    #         return resultado
    #     except Exception as e:
    #         print(str(e))
