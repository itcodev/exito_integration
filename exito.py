#!/usr/bin/python
# -*- coding: utf-8 -*-
from utilities import *
from datetime import datetime, timedelta
from factura import Factura
import requests
import json
import re
from validate_email import validate_email
import time
import traceback



def sync():

    def log_this(mensaje, unidad_negocio):
        try:
            mensaje = str(mensaje)
            hora = str(datetime.now())
            unidad_negocio = str(unidad_negocio)
            url = 'http://'+ 'localhost' + ':8080/api/bitacora'
            payload = {"Message": mensaje , "DateTime": hora ,"CompanyEntity": unidad_negocio}
            response = requests.request("POST", url, json=payload)
            return True
        except Exception as e:
            print (str(e))

    utilidades = Utilities()
    ayer = datetime.now() - timedelta(days=1)
    ayer = ayer.strftime('%d-%b-%y')
    hoy  = datetime.now().strftime('%d-%b-%y')
    facturas = utilidades.get_invoices(hoy)
    if len(facturas) == 0:
        time.sleep(0.5)
        print("SIN FACTURAS NUEVAS PARA PROCESAR " + str(datetime.now()))
        return True
    cola = int(len(facturas))
    for factura in facturas:
        print(str(cola) + " FACTURAS EN COLA")
        #se inicializa la factura electronica 
        exito_invoice = Factura()
        exito_invoice.clear_invoice()
        exito_invoice.set_companyapi("701cc020-8e06-4d35-b637-636fcc25ecea")
        # exito_invoice.set_companyapi("51bf0a66-537a-4f16-813b-ade62045d6cc")
        exito_invoice.set_environment("stag")
        # print(factura)
        ##### DATOS PARA EL KEY DE LA FACTURA #######
        #### https://github.com/itcodevelopment/alphagocore
        doc_type  = factura[0]
        voucher = factura[1]
        fecha_factura = factura[2]
        terminal = "1"
        no_transa = factura[3]
        country = "506"
        situation = "1"
        branch = factura[4]
        tipo_de_venta = factura[17]
        ###### FIN DATOS DEL KEY DE LA FACTURA #####

        ###### OBTENER LOS DATOS DEL CLIENTE #######
        cod_cliente = factura[6]
        cliente_publico = False
        # print(no_transa)
        ### TIPO DE CLIENTE
        
        try:
            customer_info =  utilidades.get_customer_info(cod_cliente)
        except Exception as e:
            print("Ha ocurrido un error al obtener los datos del cliente: {0} {1}".format(cod_cliente,str(e)))
        customer_name = customer_info[0]
        customer_identification = re.sub("[^0-9]", "", customer_info[1])
        customer_email = customer_info[3]
        customer_identification_type = customer_info[4]
        customer_identification_code = ''
        if customer_identification_type == 'CEDULA':
            customer_identification_code = '01'
        elif customer_identification_type == 'CED. RESID.':
            customer_identification_code = '03'
        elif customer_identification_type == 'CEDULA JUR.':
            customer_identification_code = '02'
        elif customer_identification_type == 'NIT':
            customer_identification_code = '04'
        elif customer_identification_type == 'PAS':
            customer_identification_code = '05'
        

        exito_invoice.set_receiver(customer_name,customer_identification_code,customer_email,customer_identification)

        if customer_name == "CLIENTE PUBLICO":
            cliente_publico = True
        ###  TIPO DE DOCUMENTO 
        if doc_type == "NC":
            document_type = "03"
        elif doc_type == "F" and cliente_publico == False:
            document_type = "01"
        elif  doc_type == "F" and cliente_publico == True:
            document_type = "04"
        else:
            print(doc_type)
            continue
        ###### FIN OBTENER EL TIPO DE DOCUMENTO #####

        ### se verifican las direcciones de correo sean validas
        if customer_email:
            email_verify = validate_email(customer_email)
        else:
            email_verify = False
        ### si los datos del usuario vienen incompletos, los envio como tiquete electronico

        if customer_name == None or customer_identification == None or customer_identification_code == None or customer_identification == None or customer_email == None or email_verify == False or cliente_publico == True:
            document_type = "04"  
            log_this("El siguiente cliente fué forzado a ser enviado como tiquete electrónico debido a que no cumplió con los parámetros suficientes para ingresar como factura. Detalles: " + str(customer_info), 'Exito Betancur')
        # SE CREA EL HEADER DE LA FACTURA
        exito_invoice.set_key(branch,terminal,document_type,no_transa,country,situation)
        # SE FINALIZA EL HEADER DE LA FACTURA

        fecha_iso_header = fecha_factura.isoformat();
        ### POR DEFAULT EL TERMINO DE CREDITO SERA 0 PARA COMPRAS A CONTADO.
        credit_term = 0
        ### FUNCIONALIDAD PARA OBTENER EL PLAZO DEL CREDITO EN DIAS
        if factura[0] == 'F' and tipo_de_venta == 'N':
            try:
                credit_term = int(utilidades.get_credit_term(no_transa))
            except Exception as e:
                print("Ha ocurrido un error al obtener los terminos de venta: {0} {1}".format(no_transa,str(e)))
        ### termino de venta
        # Contado	01
        # Crédito	02
        term_of_sale = ''
        if tipo_de_venta == 'S':
            term_of_sale = '01'
        else:
            term_of_sale = '02'
        ### MEDIOS DE PAGO
        payment_method = '01'
        #Corresponde al medio de pago empleado: 
        # 01 Efectivo, 
        # 02 Tarjeta, 
        # 03 Cheque, 
        # 04 Transferencia - depósito bancario 
        # 05 Recaudado por terceros, 
        # 99 OTROS
        pays_with_credit_card = factura[5];
        if pays_with_credit_card == 'S':
            payment_method = '02'
        
        # si es una nota de credito creo la referencia de la factura

        if document_type == "03":
            
            try:
                nc = utilidades.get_nc_reason(no_transa)
                if nc:
                    nc_clave = nc[2]
                    nc_referencia_interna = nc[1]
                    nc_razon = nc[0]
                    referencias = [{
                        "DocumentType": "01",
                        "DocumentNumber": nc_clave,
                        "Code": "99",
                        "Reason": nc_razon
                    }]
                    exito_invoice.set_reference(referencias)
                else:
                    print("Esta Nota de credito no referencia a un documento enviado anteriormente.")
                    utilidades.status('RDNEAH','RDNEAH',no_transa)
                    continue
            except Exception as e:
                print("Ha ocurrido un error al obtener los datos de esa nota de credito: {0} {1}".format(no_transa,str(e)))
        
        ### SE CREA EL HEADER DE LA FACTURA ###
        exito_invoice.set_header(fecha_iso_header,term_of_sale,credit_term,payment_method)

        redondeo = factura[13]
        total_fac = 0
        lineas = utilidades.get_header_lines(no_transa)
        for linea in lineas:
            line_number = linea[0]
            line_type = '04'
            line_code = linea[3]
            line_qty =  linea[4]
            line_unit = 'Unid'
            line_commercial_unit = None
            line_detail = linea[2]
            line_discount = 0
            line_nature_of_discount = ''
            if int(line_discount) > 0:
                line_nature_of_discount = 'DESCUENTO GENERAL'
            line_monto_imp = linea[9]
            line_monto_imp_publico = linea[12]
            line_total_bruto = linea[10]
            if line_monto_imp_publico != None:
                line_retencion = round(line_monto_imp_publico - line_monto_imp , 2)
            else:
                line_retencion = 0
            line_total_impuestos = round(line_retencion + line_monto_imp, 2)
            line_total_amount = round(line_total_bruto + line_total_impuestos, 2) 
            line_unit_price = line_total_amount / line_qty
            monto_total = round(line_qty * line_unit_price,2)
            line_subtotal = monto_total - line_discount
            monto_total_linea = line_subtotal
            total_fac = total_fac + line_total_amount

            exito_invoice.set_lineadetalle(line_number,line_type,line_code,line_qty,line_unit,line_commercial_unit,line_detail,line_unit_price,monto_total,line_discount,line_nature_of_discount,line_subtotal,monto_total_linea)
        exito_invoice.set_detalle()


        # here user information is obtained 


        # "Receiver": {  
        #     "Name": "Los patitos S.A",   
        #     "Identification": {    
        #         "Type": "02",    
        #         "Number": "3101234567"  
        #     },  
        #     "Email": "info@lospatitos.com"  
        # }




        # set summary
                # "ExchangeRate": exchangerate,
                # "TotalTaxedService": totaltaxedservice,
                # "TotalExemptService": totalexemptservice,
                # "TotalTaxedGoods": totaltaxedgoods,
                # "TotalExemptGoods": totalexemptgoods,
                # "TotalTaxed": totaltaxed,
                # "TotalExempt": totalexempt,
                # "TotalSale": totalsale,
                # "TotalDiscounts": totaldiscounts,
                # "TotalNetSale": totalnetsale,
                # "TotalTaxes": totaltaxes,
                # "TotalVoucher": totalvoucher
        exchange_rate = factura[9]
        currency = "CRC"
        total_tax_service = 0.0
        total_exempt_service = 0.0
        total_tax_goods = 0.0
        total_exempt_goods = round(total_fac,2)
        total_taxed = 0.0
        total_exempt = round(total_fac,2)
        total_sale = round(total_fac,2)
        total_discount = 0
        total_net_sale = round(total_fac - total_discount,2)
        total_taxes = 0.0
        total_voucher = round(total_fac,2)
        exito_invoice.set_summary(currency,exchange_rate,total_tax_service,total_exempt_service,total_tax_goods,total_exempt_goods,total_taxed,total_exempt,total_sale,total_discount,total_net_sale,total_taxes,total_voucher)
        exito_invoice.set_reference()
        built_invoice = exito_invoice.build_invoice()
        print(built_invoice)
        try:
            time.sleep(0.5);
            print("Enviando...")
            url = "http://localhost:8080/api/invoice/"
            headers = {
            'Content-Type': "application/json",
            'Cache-Control': "no-cache",
            }
            response = requests.request("POST", url, json=built_invoice, headers=headers, timeout=5)
            res = response.text.encode('utf-8')
            response_api = json.loads(res)
            if response_api['status'] == 2:
                print("DOCUMENTO YA HA SIDO PROCESADO")
                print(no_transa +" AT: "+ str(datetime.now()) )
                continue
            elif response_api['status'] == 1:
                clave = response_api['codificacion']['clave']
                consecutivo = response_api['codificacion']['consecutivo']
                utilidades.status(clave,consecutivo,no_transa)
                print("DOCUMENTO PROCESADO EN EL API")
                print(no_transa +" AT: "+ str(datetime.now()) )
                cola -= 1
            else:
                log_this("Sucedio un error al procesar la siguiente factura: {0} detalle del error: {1}".format(no_transa,response_api), 'Exito Betancur')
                continue
        except Exception as e:
            print("TIEMPO EXCEDIDO, MARCANDO COMO PROVISIONAL:")
            utilidades.status('PROVISIONAL','PROVISIONAL',no_transa)
            print(str(e))
            continue
    
    ## se extraen las notas de credito
    # notas_arccm = utilidades.get_arccm(hoy)
    # for nota in notas_arccm:
    #     exito_nota = Factura()
    #     exito_nota.clear_invoice()
    #     exito_nota.set_companyapi("bce17cc4-42a1-4fab-825d-affe2da24355")
    #     voucher = 'NCCXC-'+str(nota[0])
    #     fecha_factura = nota[3]
    #     terminal = "1"
    #     factura_vinculada = nota[0]
    #     country = "506"
    #     situation = "1"
    #     branch = nota[1]
    #     tipo_de_venta = factura[17]
    # return True


while True:
    try:
        sync()
    except Exception as e:
        print(str(e))
    
        




        


