#!/usr/bin/python
# -*- coding: utf-8 -*-
import re, datetime
class Factura:
    company_api = None
    environment = None
    key = {}
    header = {}
    receiver = {}
    detail = {"Detail": []}
    summary = {}
    reference = []
    other = {}
    lineas = []
    error = []
    def __init__(self):
        pass
    
    def set_companyapi(self, company_api):
        validation = re.compile("[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}", re.I)
        res = validation.match(company_api)
        if res:
            self.company_api = company_api
        else:
            self.error.append("Lo sentimos la compania no tiene un guid correcto")
        return True
    def set_environment(self, environment="stag"):
        if environment == "stag" or environment == "prod":
            self.environment = environment
            return True
        else:
            self.error.append = "Lo sentimos, los valores posibles son \"stag\" y \"prod\""
            return False
        
    def set_key(self, branch, terminal, invoice_type, voucher, country, situation):       
        if invoice_type != "04":
            self.key = {
                "Branch": branch,
                "Terminal": terminal,
                "Type": invoice_type,
                "Voucher": voucher,
                "Country": country,
                "Situation": situation,
            }
            return True
        else:
            self.key = {
                "Branch": branch,
                "Terminal": terminal,
                "Type": invoice_type,
                "Voucher": voucher,
                "Country": country,
                "Situation": situation,
            }

            self.receiver = {
                "Name":"CLIENTE TIQUETE ELECTRONICO",
                "Identification":{  
                    "Type":"01",
                    "Number":"111111111"
                },
                "Email": "tiquetes@exitocr.com"
            }

    def set_header(self, date, termofsale, creditterm, paymentmethod):
        self.header = {
            "Date": date,
            "TermOfSale": termofsale,
            "CreditTerm": creditterm,
            "PaymentMethod": paymentmethod
        }
        return True
      
    def set_receiver(self, name, code, email, number):
        
        if number:
            number = number.replace('-','')
        self.receiver = {
                "Name": name,
                "Identification": {
                    "Type": code,
                    "Number": number
                },
                "Email": email
            }
        
    def set_lineadetalle(self, number, code_type, code, quantity, unitofmeasure, commercialunitofmeasure, detail, unitprice, totalamount, discount, natureofdiscount, subtotal, totallineamount, tax=[]):
        if not tax:
            linea = {
                "Number": number,
                "Code": {
                    "Type": code_type,
                    "Code": code
                },
                "Quantity": quantity,
                "UnitOfMeasure": unitofmeasure,
                "CommercialUnitOfMeasure": commercialunitofmeasure,
                "Detail": detail,
                "UnitPrice": unitprice,
                "TotalAmount": totalamount,
                "Discount": discount,
                "NatureOfDiscount": natureofdiscount,
                "SubTotal": subtotal,
                "Tax": None,
                "TotalLineAmount": totallineamount
            }
            self.lineas.append(linea)
        else:
            linea = {
                "Number": number,
                "Code": {
                    "Type": code_type,
                    "Code": code
                },
                "Quantity": quantity,
                "UnitOfMeasure": unitofmeasure,
                "CommercialUnitOfMeasure": commercialunitofmeasure,
                "Detail": detail,
                "UnitPrice": unitprice,
                "TotalAmount": totalamount,
                "Discount": discount,
                "NatureOfDiscount": natureofdiscount,
                "SubTotal": subtotal,
                "Tax": tax,
                "TotalLineAmount": totallineamount
            }
            self.lineas.append(linea)

    def set_detalle(self):
        self.detail = self.lineas
        
    
    def set_summary(self, currency, exchangerate, totaltaxedservice, totalexemptservice, totaltaxedgoods, totalexemptgoods, totaltaxed, totalexempt, totalsale, totaldiscounts, totalnetsale, totaltaxes, totalvoucher):
        self.summary = {
            "Currency": currency,
		    "ExchangeRate": exchangerate,
		    "TotalTaxedService": totaltaxedservice,
		    "TotalExemptService": totalexemptservice,
		    "TotalTaxedGoods": totaltaxedgoods,
		    "TotalExemptGoods": totalexemptgoods,
		    "TotalTaxed": totaltaxed,
		    "TotalExempt": totalexempt,
		    "TotalSale": totalsale,
		    "TotalDiscounts": totaldiscounts,
		    "TotalNetSale": totalnetsale,
		    "TotalTaxes": totaltaxes,
		    "TotalVoucher": totalvoucher
        }
        return True
    def set_reference(self, references=[]):
        if not references:
            return True
        else:
            self.reference = references
            return True
    

    def build_invoice(self):
        invoice = {
	        "CompanyAPI": self.company_api,
	        "Environment": self.environment,
	        "Key": self.key,
            "Header": self.header,
            "Receiver": self.receiver,
            "Detail": self.detail,
            "Summary": self.summary,
            "Reference": self.reference,
            "Other": None
        }
        return invoice     
    def clear_invoice(self):
        self.company_api = None
        self.environment = None
        self.key = {}
        self.header = {}
        self.receiver = {}
        self.detail = {"Detail": []}
        self.summary = {}
        self.reference = []
        self.other = {}
        self.lineas = []
        self.error = []   
